﻿using System.Text;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Ning.Tools;
using LitJson;
using System.IO;

public class EditMap : MonoBehaviour {
    public Toggle toggle;
    public Button btn_Save, btn_SaveAll, btn_delete,btn_shang,btn_xia;
    public Text txt_mapIndex;
    public InputField Input_index;
    List<Toggle> toggles;
    List<string> maps;
    int mapIndex = 0;
    // Use this for initialization
    void Start () {
        toggles = new List<Toggle>();
        maps = new List<string>();
        var oringePos = toggle.transform.localPosition;
        for (int y = 0; y <= 15; y++)
        {
            for (int x = 0; x <= 18; x++)
            {
                var newToggle = Object.Instantiate(toggle.gameObject, toggle.transform.parent);
                newToggle.transform.localPosition = new Vector3(oringePos.x + x * 50f , oringePos.y - y * 23f );
                toggles.Add(newToggle.GetComponent<Toggle>());
            }
        }
        toggle.gameObject.SetActive(false);
        btn_Save.onClick.AddListener(SaveData);
        btn_SaveAll.onClick.AddListener(SaveAll);
        btn_delete.onClick.AddListener(Delete);
        btn_shang.onClick.AddListener(LoadShang);
        btn_xia.onClick.AddListener(LoadXia);
        Input_index.onEndEdit.AddListener(EditIndex);
        LoadData();
        LoadMap();
        SetMapIndexText();

    }
    
    void SaveData()
    {
        var data = new StringBuilder();
        for (int i = 0; i < toggles.Count; i++)
        {
            data.Append(toggles[i].isOn ? 1 : 0);
        }
        Debug.Log(data.ToString());
        if (maps.Count == 0)
        {
            maps.Add("");
        }
        maps[mapIndex] = data.ToString();

    }

    void SaveAll()
    {
        var data = new StringBuilder();

        for (int i = 0; i < maps.Count; i++)
        {
            if (!string.IsNullOrEmpty(maps[i]))
            {
                data.AppendFormat("{0}|", maps[i]);
            }
        }

        SaveLoadManager.Save(data.ToString(), Global.mapFileName, Global.dataFolderName);
        Debug.Log("Save Success");
    }

    void LoadData()
    {
        var tmp = SaveLoadManager.Load(Global.mapFileName, Global.dataFolderName) as string;
        if (string.IsNullOrEmpty(tmp) == false)
        {
            var data = tmp.Split('|');
            for (int i = 0; i < data.Length; i++)
            {
                if (!string.IsNullOrEmpty(data[i]))
                {
                    maps.Add(data[i]);
                }
            }
        }
        if (maps.Count == 0)
        {
            maps.Add("");
        }
    }
    void LoadMap()
    {
        for (int i = 0; i < toggles.Count; i++)
        {
            toggles[i].isOn =  maps[mapIndex] != "" && maps[mapIndex][i] == '1';
        }
    }

    void LoadXia()
    {
        mapIndex++;
        if (mapIndex == maps.Count)
        {
            maps.Add("");
        }
        LoadMap();
        SetMapIndexText();
    }

    void LoadShang()
    {
        mapIndex--;
        if (mapIndex < 0)
        {
            mapIndex = 0;
        }
        LoadMap();
        SetMapIndexText();
    }
    void EditIndex(string _index)
    {
        int index = -1;
        if (int.TryParse(_index,out index))
        {
            var current_data = maps[mapIndex];
            if (index > maps.Count)
            {
                for (int i = maps.Count; i <= index; i++)
                {
                    maps.Add("");
                }
                maps[mapIndex] = "";
                maps[index - 1] = current_data;


            }else
            {
                maps[mapIndex] = maps[index - 1];
                maps[index - 1] = current_data;
            }
            mapIndex = index - 1;
            SetMapIndexText();
        }
    }
    void Delete()
    {
        maps.RemoveAt(mapIndex);
        if (maps.Count == 0)
        {
            maps.Add("");
        }
        LoadShang();
    }
    void SetMapIndexText()
    {
        //txt_mapIndex.text = string.Format("index: {0}\n count: {1}", mapIndex+1 ,maps.Count.ToString());
        txt_mapIndex.text = string.Format("count: {0}",maps.Count.ToString());
        Input_index.text = (mapIndex + 1).ToString();
    }
}
