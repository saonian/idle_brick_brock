﻿using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
namespace BlockBreak
{
    public class Collider : MonoBehaviour {

        public bool Value = false;
        public BallType ColliderBallType;
        public ColliderType SelfType;
        //[HideInInspector]
        public ColliderType ColliderType = ColliderType.Null;
        void OnTriggerEnter2D(Collider2D col)
        {
            if (SelfType == ColliderType.Block)
            {
                var ball = col.gameObject.GetComponent<Ball>();
                var block = gameObject.GetComponent<Block>();
                if (ball)
                {
                    if (block)
                    {
                        ball.Power = Global.GetBallPowerLevel(ball.BallType)+1;
                        block.Damage = ball.Power;
                    }
                    ball.Target = this.transform;
                    ColliderBallType = ball.BallType;
                }
            }
            else if (SelfType == ColliderType.Wall)
            {
                var BallMove = col.gameObject.GetComponent<BallMove>();
                var Heading2D = col.gameObject.GetComponent<Heading2D>();

                var goingLeft = BallMove.goingLeft;
                var goingDown = BallMove.goingDown;
                var direction = Heading2D.Value;

                if (!goingLeft)
                {
                    direction = new Vector2(-direction.x, direction.y);
                    goingLeft = true;
                }
                if ( goingLeft)
                {
                    direction = new Vector2(-direction.x, direction.y);
                    goingLeft = false;
                }
                if (!goingDown)
                {
                    direction = new Vector2(direction.x, -direction.y);
                    goingDown = true;
                }
                if (goingDown)
                {
                    direction = new Vector2(direction.x, -direction.y);
                    goingDown = false;
                }

                BallMove.goingLeft = goingLeft;
                BallMove.goingDown = goingDown;
                Heading2D.Value = direction;
            }
        }
    }
}
