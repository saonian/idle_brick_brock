﻿using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;
namespace BlockBreak
{
    public class SniperBallSkillSystem : ComponentSystem
    {
        [Inject] private BlockData block_data;
        [Inject] private BallData ball_data;

        /// <summary>
        /// 处理砖块碰撞分数计算
        /// </summary>
        protected override void OnUpdate()
        {
            if (ball_data.Length == 0)
            {
                return;
            }
            for (int index = 0; index < ball_data.Length; ++index)
            {
                //if (ball_data.Collider[index] != null 
                //    && ball_data.Collider[index].Value 
                //    && ball_data.Ball[index].BallType == BallType.SniperBall 
                //    && ball_data.Collider[index].ColliderType == ColliderType.Wall)
                //{
                //    var blockPos = SetBallDirection(ball_data.Position[index].Value);
                //    if (blockPos != null)
                //    {
                //        ball_data.Collider[index].Value = false;
                //        ball_data.Heading2D[index].Value = blockPos.Value -ball_data.Position[index].Value;
                //    }

                //}
            }
        }

        private float2? SetBallDirection(float2 pos)
        {
            if (block_data.Length == 0)
            {
                return null;
            }
            float minDistance = float.MaxValue;
            int index = 0;
            for (int i = 0; i < block_data.Length; i++)
            {
                var blockPos = block_data.Position[i].Value;

                var distance = math.distance(pos, blockPos);

                if (distance < minDistance)
                {
                    minDistance = distance;
                    index = i;
                }
            }
            if (block_data.Position[index] != null)
            {
                return block_data.Position[index].Value;
            }
            else
            {
                return null;
            }
        }
    }
}

