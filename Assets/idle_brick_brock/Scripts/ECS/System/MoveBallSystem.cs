﻿using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;
namespace BlockBreak
{
    public struct BallData
    {
        public readonly int Length;
        public GameObjectArray GameObject;
        public ComponentArray<Ball> Ball;
        public ComponentArray<BallMove> BallMove;
        public ComponentArray<Heading2D> Heading2D;
        public ComponentArray<Rigidbody2D> Rigidbody2D;
    }
    
    public class MoveBallSystem : ComponentSystem
    {

        [Inject] private BallData m_Data;

        public float baseSpeed = 35f;//球的移动速度基础系数

        protected override void OnUpdate()
        {
            if (m_Data.Length == 0)
                return;
            Move(m_Data);
            SetDirection(m_Data);
        }

        private void Move(BallData data)
        {
            for (int index = 0; index < data.Length; ++index)
            {
                var Heading2D = data.Heading2D[index].Value;
                var Speed = data.BallMove[index].Speed;
                // var SpeedLevel = data.BallMove[index].SpeedLevel;
                var SpeedLevel = Global.GetBallSpeedLevel(data.Ball[index].BallType);
                data.BallMove[index].SpeedLevel = SpeedLevel;
                data.Rigidbody2D[index].velocity = Heading2D * (Speed * baseSpeed + Speed*SpeedLevel*15) * Time.deltaTime;
            }
        }

        private void SetDirection(BallData data)
        {
            for (int index = 0; index < data.Length; ++index)
            {
                var Ball = data.Ball[index];
                var position = data.GameObject[index].transform.position;
                var BallMove = data.BallMove[index];

                if (Ball.Target != null)
                {
                    Vector2 dir = new Vector2();
                    dir = (Vector2)position - new Vector2(Ball.Target.position.x, Ball.Target.position.y);      
                    dir.Normalize();

                    if (dir.x > 0)
                        BallMove.goingLeft = false;
                    if (dir.x < 0)
                        BallMove.goingLeft = true;
                    if (dir.y > 0)
                        BallMove.goingDown = false;
                    if (dir.y < 0)
                        BallMove.goingDown = true;
                    Ball.Target = null;
                    data.Heading2D[index].Value = dir;
                }
                var goingLeft = BallMove.goingLeft;
                var goingDown = BallMove.goingDown;
                var direction = data.Heading2D[index].Value;

                if (position.x > Bootstrap.Settings.playfield.x && !goingLeft)
                {                   
                    direction = new Vector2(-direction.x, direction.y);     
                    goingLeft = true;                                       
                }
                if (position.x < -Bootstrap.Settings.playfield.x && goingLeft)
                {                   
                    direction = new Vector2(-direction.x, direction.y);     
                    goingLeft = false;                                      
                }
                if (position.y > Bootstrap.Settings.playfield.y && !goingDown)
                {                   
                    direction = new Vector2(direction.x, -direction.y);     
                    goingDown = true;                                       
                }
                if (position.y < -Bootstrap.Settings.playfield.y && goingDown)
                {
                    direction = new Vector2(direction.x, -direction.y);
                    goingDown = false;
                }
                BallMove.goingLeft = goingLeft;
                BallMove.goingDown = goingDown;
                data.Heading2D[index].Value = direction;
            }
        }
    }
}
