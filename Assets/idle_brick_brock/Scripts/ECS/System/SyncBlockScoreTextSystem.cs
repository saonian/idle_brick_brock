﻿using TMPro;
//using Unity.Collections.Generic;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace BlockBreak
{
    public static class SyncBlockScoreTextSystem
    {
       
        public static Dictionary<Block,TextMeshPro> dictionary = new Dictionary<Block, TextMeshPro>();

        public static void InitDictionary(Block block)
        {
            if (block)
            {
                var text = block.GetComponentInChildren<TextMeshPro>();
                if (text)
                {
                    dictionary.Add(block, text);
                }
            }
        }

        public static void SetBlockText(Block block)
        {
            if (dictionary.ContainsKey(block))
            {
                dictionary[block].text = block.Score.ToString();
            }
        }
    }

}

