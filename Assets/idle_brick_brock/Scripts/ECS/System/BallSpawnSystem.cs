﻿using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using Random = UnityEngine.Random;
using Unity.Entities;
using UnityEngine;

namespace BlockBreak
{
    public struct BallCountData
    {
        public readonly int Length;
        public ComponentArray<BallCount> data;
    }
    public class BallSpawnSystem : ComponentSystem
    {
        private int[] currentBallsCount;
        private List<GameObject> balls = new List<GameObject>();
        [Inject] private BallCountData m_Data;
        protected override void OnUpdate()
        {
            if (currentBallsCount == null)
            {
                currentBallsCount = new int[]{0,0,0,0,0,0};
            }

            if (m_Data.Length == 0)
            return;

            if (m_Data.data[0] != null)
            {
                var m_count = m_Data.data[0].Count;
                for (int i = 0; i < m_count.Length; i++)
                {
                    if (currentBallsCount[i] != m_count[i])
                    {
                        SyncBallCount((BallType)i ,currentBallsCount[i] , m_count[i]);
                    }
                }
            }
        }

        private void SyncBallCount(BallType BallType,int currentCount,int targetCount)
        {
            if (targetCount > currentCount)
            {
                for (int i = currentCount; i < targetCount; i++)
                {
                    // U.L(i);
                    Spawn(BallType);
                }
            }
            else if (targetCount < currentCount)
            {
                    U.L("Remove");

                for (int i = targetCount; i < currentCount; i++)
                {
                    Remove(BallType);
                }
            }
        }

        private void Spawn(BallType BallType)
        {
            // U.L("Spawn",BallType);
            var Position = new float2(0, 0);
            var Heading = new float2(Random.Range(-1f, 1f), Random.Range(-1f, 1f));

            var settings = Bootstrap.Settings;
            var prefab = settings.BallPrefabs[(int)BallType];
            var newBall = Object.Instantiate(prefab, prefab.transform.parent);
            newBall.transform.position = new Vector3(Position.x, Position.y);
            var shotHeading = newBall.GetComponent<Heading2D>();
            Vector2 dir = new Vector2(Heading.x, Heading.y);
            dir.Normalize();
            shotHeading.Value = new float2(dir.x, dir.y);

            var BallMove = newBall.GetComponent<BallMove>();

            if (Heading.x > 0)
                BallMove.goingLeft = false;
            if (Heading.x < 0)
                BallMove.goingLeft = true;
            if (Heading.y > 0)
                BallMove.goingDown = false;
            if (Heading.y < 0)
                BallMove.goingDown = true;
            newBall.SetActive(true);
            balls.Add(newBall);
            currentBallsCount[(int)BallType]++;
        }

        private void Remove(BallType BallType)
        {

            for (int i = 0; i < balls.Count; i++)
            {
                var ball = balls[i].GetComponent<Ball>();
                if (ball && ball.BallType == BallType)
                {
                    balls.RemoveAt(i);
                    ball.gameObject.SetActive(false);
                    Object.Destroy(ball.gameObject);
                    currentBallsCount[(int)BallType]--;
                    return;
                }
            }
        }
    }
}
