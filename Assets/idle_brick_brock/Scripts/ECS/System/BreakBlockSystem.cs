﻿using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;
namespace BlockBreak
{
    public struct BlockData
    {
        public readonly int Length;
        public GameObjectArray GameObject;
        public ComponentArray<Position2D> Position;
        public ComponentArray<Block> Block;
        public ComponentArray<Collider> Collider;
    }

    public class BreakBlockSystem : ComponentSystem
    {
        [Inject] private BlockData m_Data;

        /// <summary>
        /// 处理砖块碰撞分数计算
        /// </summary>
        protected override void OnUpdate()
        {
            if (m_Data.Length == 0)
                return;
            var totalScore = 0;
            for (int index = 0; index < m_Data.Length; ++index)
            {
                if (m_Data.Collider[index] != null)
                {
                    m_Data.Collider[index].Value = false;
                    m_Data.Block[index].Score -= m_Data.Block[index].Damage;
                    totalScore += m_Data.Block[index].Damage;
                    m_Data.Block[index].Damage = 0;
                    SyncBlockScoreTextSystem.SetBlockText(m_Data.Block[index]);
                    if (m_Data.Block[index].Score <= 0)
                    {
                        m_Data.GameObject[index].SetActive(false);
                        MapManager.Instance.CheckMap();
                    }
                }
            }

            if (totalScore != 0)
            {
                ScoreEvent.Trigger(totalScore);
            }
        }
    }
}

