﻿using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace BlockBreak
{
    [UpdateAfter(typeof(MoveBallSystem))]
    public class SyncColorSystem : ComponentSystem
    {
        public struct Data
        {
            public readonly int Length;
            public GameObjectArray GameObject;
            public ComponentArray<Block> Block;
            public ComponentArray<SpriteRenderer> SpriteRenderer;
        }


        [Inject] private Data m_Data;

        protected override void OnUpdate()
        {
            if (m_Data.Length == 0)
                return;

            for (int index = 0; index < m_Data.Length; ++index)
            {
                var colorType = m_Data.Block[index].Score % (Bootstrap.Settings.blockColors.Length-1);

                m_Data.SpriteRenderer[index].color = Bootstrap.Settings.blockColors[colorType];
            }
        }
    }

}

