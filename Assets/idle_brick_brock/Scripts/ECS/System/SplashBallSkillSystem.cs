﻿using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;
namespace BlockBreak
{
    [UpdateBefore(typeof(BreakBlockSystem))]
    public class SplashBallSkillSystem : ComponentSystem
    {

        private int splashRange = 1;//扩散半径 TODO:升级选项,后面要改为动态的
        private int splashScore = 1;//扩散分数 TODO:升级选项,后面要改为动态的
        private int width = 16;
        private int length = 19;



        //public struct BlockData
        //{
        //    public readonly int Length;
        //    public GameObjectArray GameObject;
        //    public ComponentArray<Position2D> Position;
        //    public ComponentArray<Block> Block;
        //    public ComponentArray<Collider> Collider;
        //}

        [Inject] private BlockData m_Data;

        /// <summary>
        /// 处理溅射球的碰撞效果
        /// </summary>
        protected override void OnUpdate()
        {
            if (m_Data.Length == 0)
                return;
            for (int index = 0; index < m_Data.Length; ++index)
            {
                if (m_Data.Collider[index].ColliderBallType == BallType.SplashBall)
                {
                    SetSplashBlock(m_Data.GameObject[index]);
                    m_Data.Collider[index].ColliderBallType = BallType.None;
                }
            }
        }


        private void SetSplashBlock( GameObject block)
        {
            var blockIndex = MapManager.Instance.GetBlockIndex(block);
            if (blockIndex != -1)
            {
                int left_up_index = blockIndex - length * splashRange - splashRange;
                int right_dowm_index = left_up_index + (length + 1) * splashRange * 2;
                var column = blockIndex / length;
                var count = 0;
                for (int i = 0; i < splashRange * 2 + 1; i++)
                {
                    var i_column = i - splashRange + column;
                    for (int i2 = 0; i2 < splashRange * 2 + 1; i2++)
                    {
                        int a = left_up_index + length * i + i2;
                        if (a < 0 || a >= MapManager.Instance.Blocks.Length
                            || i_column != a / length)
                        {
                            continue;
                        }
                        else
                        {
                            var _block = MapManager.Instance.Blocks[a].GetComponent<Block>();
                            if (_block && _block.gameObject.activeSelf && _block != block && CanSplash(count))
                            {
                                var score = splashScore;
                                if (_block.Score - score < 0)
                                {
                                    score = 0;
                                    _block.Score = 0;
                                }
                                else
                                {
                                    _block.Score -= score;
                                }
                                U.L("=", _block);
                                ScoreEvent.Trigger(score);
                                SyncBlockScoreTextSystem.SetBlockText(_block);
                                count++;
                            }
                        }
                    }
                }
            }
        }

        //溅射球等级限制溅射的数量 TODO:溅射球升级相关
        private bool CanSplash(int count)
        {
            //暂时溅射2个球
            var num = 2;
            return count < num;
        }
    }
}

