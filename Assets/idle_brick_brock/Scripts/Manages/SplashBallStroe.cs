namespace BlockBreak
{
    /// <summary>
    /// 基础球升级窗口
    /// </summary>
    /// TODO:配置数据
    public class SplashBallStroe : IStore
    {
        public BallPriceData priceData { get; set; }

        public bool BuyBall()
        {
            var ballData = Global.GetBallData(BallType.BasicBall);
            if (ballData != null)
            {
                var price = BuyPrice();
                if (CanUpgrader(price))
                {
                    return false;//不能购买
                }
                else
                {
                    Global.userGameData.Score -= price;
                    ballData.Count += 1;
                    return true;
                }
            }
            return false;
        }

        public bool UpgraderSpeed()
        {

            var ballData = Global.GetBallData(BallType.BasicBall);
            if (ballData != null)
            {
                var price = SpeedPrice();
                if (CanUpgrader(price))
                {
                    return false;//不能购买
                }
                else
                {
                    Global.userGameData.Score -= price;
                    ballData.SpeedLevel += 1;
                    return true;
                }
            }
            return false;
        }
        public bool UpgraderPower()
        {
            var ballData = Global.GetBallData(BallType.BasicBall);
            if (ballData != null)
            {
                var price = PowerPrice();
                if (CanUpgrader(price))
                {
                    return false;//不能购买
                }
                else
                {
                    Global.userGameData.Score -= price;
                    ballData.PowerLevel += 1;
                    return true;
                }
            }
            return false;
        }

        public int BuyPrice()
        {
            var ballData = Global.GetBallData(BallType.BasicBall);
            if (ballData != null)
            {
                var count = ballData.Count;
                var baseCost = priceData.buyPrice.baseCost;
                var costIncrease = priceData.buyPrice.costIncrease;
                var price = StoreManager.Instance.Price(count,baseCost,costIncrease);
                return price;
            }
            return int.MinValue;
        }

        public int PowerPrice()
        {
            var ballData = Global.GetBallData(BallType.BasicBall);
            if (ballData != null)
            {
                var level = ballData.PowerLevel;
                var baseCost = priceData.power.baseCost;
                var costIncrease = priceData.power.costIncrease;
                var price = StoreManager.Instance.Price(level,baseCost,costIncrease);
                return price;
            }
            return int.MinValue;
        }

        public int SpeedPrice()
        {
            var ballData = Global.GetBallData(BallType.BasicBall);
            if (ballData != null)
            {
                var level = ballData.PowerLevel;
                var baseCost = priceData.power.baseCost;
                var costIncrease = priceData.power.costIncrease;
                var price = StoreManager.Instance.Price(level,baseCost,costIncrease);
                return price;
            }
            return int.MinValue;
        }



        public bool CanUpgrader(int price)
        {
            return price <= Global.userGameData.Score;
        }
        

    }
}