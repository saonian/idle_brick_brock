﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Ning.Tools;
using Unity.Mathematics;
using LitJson;
using Random = Unity.Mathematics.Random;

namespace BlockBreak
{
    public class StoreManager : Singleton<StoreManager>
    {
        private BallCount ballCount;
        public List<Price> ball_price { get; set; }
        // private BasicBallStroe BasicStroe;
        private BaseStore basicStroe;
        private BaseStore splashStroe;

        protected override void Init()
        {
            ballCount = transform.Find("ballCount").GetComponent<BallCount>();
            LoadData();
        }


        /// <summary>
        /// 根据类型购买球
        /// </summary>
        public bool BuyBall(BallType ballType)
        {
            var store = GetStore(ballType);
            var success = store.BuyBall();
            if (success)
            {
                //更新球数量的数据,生产出球
                ballCount.Count[(int)ballType]++;
            }
            return success;
        }

        public bool DeleteBall(BallType ballType)
        {
            var store = GetStore(ballType);
            var success = store.DeleteBall();
            if (success)
            {
                //更新球数量的数据,生产出球
                ballCount.Count[(int)ballType]--;
            }
            return success;
        }

        /// <summary>
        /// 获取当前球的价格
        /// </summary>
        public int GetBallPrice(BallType ballType)
        {
            var store = GetStore(ballType);
            var price = store.BuyPrice();
            return price;
        }

        /// <summary>
        /// 获取当前球升级速度的价格
        /// </summary>
        public int GetUpgraderSpeedPrice(BallType ballType)
        {
            var store = GetStore(ballType);
            var price = store.SpeedPrice();
            return price;
        }

        /// <summary>
        /// 获取当前球升级力量的价格
        /// </summary>
        public int GetUpgraderPowerPrice(BallType ballType)
        {
            var store = GetStore(ballType);
            var price = store.PowerPrice();
            return price;
        }

        public bool UpgraderSpeed(BallType ballType)
        {
            var store = GetStore(ballType);
            return store.UpgraderSpeed();
        }
        public bool UpgraderPower(BallType ballType)
        {
            var store = GetStore(ballType);
            return store.UpgraderPower();
        }


        /// <summary>
        /// 价格计算公式
        /// </summary>
        public int Price(int count,int baseCost,float costIncrease)
        {
            var price = (int)Mathf.Round(baseCost* Mathf.Pow(costIncrease, count));
            return price;
        }

        
        public void LoadBall()
        {
            ballCount.Count = new int[]{0,0,0,0,0,0};
            // for (int i = 0; i < ballCount.Count.Length; i++)//TODO:目前只有两个球的数据
            for (int i = 0; i < 1; i++)
            {
                // U.L(ballCount.Count[0],Global.GetBallData((BallType)i).Count);
                ballCount.Count[0] = Global.GetBallData((BallType)i).Count;
            }
        }

        public void Restart()
        {
            LoadBall();
        }

#region private
        /// <summary>
        /// 获取商品服务窗口
        /// </summary>
        private BaseStore GetStore(BallType ballType)
        {
            switch (ballType)
            {
                case BallType.BasicBall:
                    return basicStroe;
                case BallType.SplashBall:
                    return splashStroe;
                // case BallType.SniperBall:
                //     return sniperBallData;
                // case BallType.ScatterBall:
                //     return basicBallData;
                // case BallType.CannonBall:
                //     return basicBallData;
                // case BallType.CancerBall:
                //     return basicBallData;
                default:
                    return null;
            }
        }
        /// <summary>
        /// 加载商品配置数据
        /// </summary>
        private void LoadData()
        {
            var json = SaveLoadManager.Load(Global.dataFileName, Global.dataFolderName) as string;
            if (string.IsNullOrEmpty(json) == false)
            {
                U.L(json);
                JsonReader reader = new JsonReader(json);
                JsonData jsonData = JsonMapper.ToObject(reader);

                JsonData j_Basic = jsonData["basic_price"];
                basicStroe = BuildStore(BallType.BasicBall,j_Basic);

                JsonData j_splash = jsonData["splash_price"];
                splashStroe = BuildStore(BallType.SplashBall,j_splash);
            }
        }

        private BaseStore BuildStore(BallType ballType,JsonData data)
        {
            JsonReader r_js = new JsonReader(data.ToJson());
            var priceData = JsonMapper.ToObject<BallPriceData>(r_js);
            // U.L(ballType,priceData.buyPrice.baseCost,priceData.speed.baseCost);
            return new BaseStore(ballType,priceData);
        }
    }
#endregion
}
