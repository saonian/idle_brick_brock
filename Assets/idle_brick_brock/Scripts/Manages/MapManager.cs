﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Ning.Tools;
using Unity.Mathematics;
namespace BlockBreak
{
    public class MapManager : Singleton<MapManager>
    {
        List<string> maps;
        private GameObject[] blocks;

        public GameObject[] Blocks
        {
            get
            {
                return blocks;
            }
        }
        /// <summary>
        /// 地图等级(从1开始)
        /// </summary>
        private int level;

        public int Level
        {
            get
            {
                return level;
            }
        }

        protected override void Init()
        {
            level = Global.userGameData.Level;
            LoadMapData();
        }

        /// <summary>
        /// 加载地图
        /// </summary>
        public void LoadMap()
        {
            LevelEvent.Trigger(level);
            int mapIndex = (level-1)%maps.Count;
            // Debug.Log("mapIndex: "+mapIndex);
            if (blocks == null || blocks.Length == 0)
            {
                blocks = new GameObject[maps[1].Length+1];//TODO:待优化,使用ECS的数据结构
                float2 pos = new float2(-15.2f, 6.6f);
                float2 distance = new float2(1.6f, 0.4f);
                var blockIndex = 0;
                for (int y = 0; y <= 15; y++)
                {
                    for (int x = 0; x <= 18; x++)
                    {
                        var settings = Bootstrap.Settings;
                        var prefab = settings.BlockPrefab;
                        var newBlock = Object.Instantiate(prefab, prefab.transform.parent);
                        newBlock.name = string.Format("({0},{1})", x, y);
                        newBlock.transform.localPosition = new Vector3(pos.x + x * distance.x, pos.y - y * distance.y,0);
                        var block = newBlock.GetComponent<Block>();
                        if (block)
                        {
                            SyncBlockScoreTextSystem.InitDictionary(block);
                            SyncBlockScoreTextSystem.SetBlockText(block);
                        }
                        blocks[blockIndex] = newBlock;
                        blockIndex++;
                    }
                }
            }
            for (int i = 0; i < blocks.Length - 1; i++)
            {
                var isActive = maps[mapIndex] != "" && maps[mapIndex][i] == '1';
                var obj = blocks[i];
                obj.SetActive(isActive);
                var block = obj.GetComponent<Block>();
                if (block)
                {
                    block.Score = level;
                    SyncBlockScoreTextSystem.SetBlockText(block);
                }
            }

        }
        /// <summary>
        /// 检查地图，砖块打完时需要重新加载地图
        /// </summary>
        public void CheckMap()
        {
            for (int i = 0; i < blocks.Length; i++)
            {
                if (blocks[i].gameObject.activeSelf)
                {
                    return;
                }
            }
            LevelUp();
        }

        /// <summary>
        /// 地图等级提升
        /// </summary>
        public void LevelUp()
        {
            level++;
            Global.userGameData.Level = level;
            Global.Save();
            LoadMap();
        }

        /// <summary>
        /// 重置地图等级
        /// </summary>
        public void Restart()
        {
            level = 1;
            Global.userGameData.Level = level;
            Global.Save();
        }

        public int GetBlockIndex(GameObject block)
        {
            for (int i = 0; i < blocks.Length; i++)
            {
                if (blocks[i] == block)
                {
                    return i;
                }
            }
            return -1;
        }

        private void LoadMapData()
        {
            maps = new List<string>();
            var tmp = SaveLoadManager.Load(Global.mapFileName, Global.dataFolderName) as string;
            if (string.IsNullOrEmpty(tmp) == false)
            {
                var data = tmp.Split('|');
                for (int i = 0; i < data.Length; i++)
                {
                    if (!string.IsNullOrEmpty(data[i]))
                    {
                        maps.Add(data[i]);
                    }
                }
            }
        }
    }
}
