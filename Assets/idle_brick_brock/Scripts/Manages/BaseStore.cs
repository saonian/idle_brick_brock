namespace BlockBreak
{
    /// <summary>
    /// 商店抽象类
    /// </summary>
    public class BaseStore
    {
        public BallType type;
        public BallPriceData priceData;
        public BaseStore(BallType _ballType,BallPriceData _pricedata)
        {
            type = _ballType;
            priceData = _pricedata;
        }

        public bool BuyBall()
        {
            var ballData = Global.GetBallData(type);
            if (ballData != null)
            {
                var price = BuyPrice();
                if (CanBuy(price) == false)
                {
                    return false;//不能购买
                }
                else
                {
                    Global.userGameData.Score -= price;
                    ballData.Count++;
                    return true;
                }
            }
            return false;
        }

        public bool DeleteBall()
        {
            var ballData = Global.GetBallData(type);
            if (ballData != null && ballData.Count > 0)
            {
                ballData.Count--;
                RefreshEvent.Trigger();
                return true;
            }
            return false;
        }

        public bool UpgraderSpeed()
        {

            var ballData = Global.GetBallData(type);
            if (ballData != null)
            {
                var price = SpeedPrice();
                if (CanBuy(price) == false)
                {
                    return false;//不能购买
                }
                else
                {
                    Global.userGameData.Score -= price;
                    ballData.SpeedLevel += 1;
                    return true;
                }
            }
            return false;
        }
        public bool UpgraderPower()
        {
            var ballData = Global.GetBallData(type);
            if (ballData != null)
            {
                var price = PowerPrice();
                if (CanBuy(price) == false)
                {
                    return false;//不能购买
                }
                else
                {
                    Global.userGameData.Score -= price;
                    ballData.PowerLevel += 1;
                    return true;
                }
            }
            return false;
        }

        public int BuyPrice()
        {
            var ballData = Global.GetBallData(type);
            if (ballData != null)
            {
                var count = ballData.Count;
                var baseCost = priceData.buyPrice.baseCost;
                var costIncrease = priceData.buyPrice.costIncrease;
                var price = StoreManager.Instance.Price(count,baseCost,costIncrease);
                return price;
            }
            return int.MinValue;
        }


        public int SpeedPrice()
        {
            var ballData = Global.GetBallData(type);
            if (ballData != null)
            {
                var level = ballData.SpeedLevel;
                var baseCost = priceData.speed.baseCost;
                var costIncrease = priceData.speed.costIncrease;
                var price = StoreManager.Instance.Price(level,baseCost,costIncrease);
                return price;
            }
            return int.MinValue;
        }
        public int PowerPrice()
        {
            var ballData = Global.GetBallData(type);
            if (ballData != null)
            {
                var level = ballData.PowerLevel;
                var baseCost = priceData.power.baseCost;
                var costIncrease = priceData.power.costIncrease;
                var price = StoreManager.Instance.Price(level,baseCost,costIncrease);
                return price;
            }
            return int.MinValue;
        }
        
        public bool CanBuy(int price)
        {
            return price <= Global.userGameData.Score;
        }
    }
}