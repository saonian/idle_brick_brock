namespace BlockBreak
{
    /// <summary>
    /// 商店窗口
    /// </summary>
    public interface IStore
    {
        /// <summary>
        /// 升级速度的商店处理窗口
        /// </summary>
        bool UpgraderSpeed();
        /// <summary>
        /// 升级力量的商店处理窗口
        /// </summary>
        bool UpgraderPower();

        /// <summary>
        /// 购买球
        /// </summary>
        bool BuyBall();
        int BuyPrice();
        int SpeedPrice();
        int PowerPrice();



        bool CanUpgrader(int price);
    }
}