﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace BlockBreak
{
    /// <summary>
    /// 球的购买价格配置
    /// </summary>
    public class Price
    {
        /// <summary>
        /// 基础成本
        /// </summary>
        public int baseCost { get; set; }
        /// <summary>
        /// 成本增量
        /// </summary>
        public float costIncrease { get; set; }
    }
}

