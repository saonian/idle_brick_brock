using Ning.Preferences;

namespace BlockBreak
{
    /// <summary>
    /// 球的类型
    /// </summary>
    public enum BallType
    {
        /// <summary>
        /// 基础球,撞击砖块和墙时会反弹
        /// </summary>
        BasicBall = 0,
        /// <summary>
        /// 溅射球,撞击砖块时会触发周围的砖块
        /// </summary>
        SplashBall = 1,
        /// <summary>
        /// 狙击球
        /// </summary>
        SniperBall = 2,
        /// <summary>
        /// 分散球
        /// </summary>
        ScatterBall = 3,
        /// <summary>
        /// 大炮球
        /// </summary>
        CannonBall = 4,
        /// <summary>
        /// 癌球
        /// </summary>
        CancerBall = 5,
        /// <summary>
        /// 小球
        /// </summary>
        SmallBall = 6,
        None = 10,
    }

    /// <summary>
    /// 碰撞体类型
    /// </summary>
    public enum ColliderType
    {
        Ball,
        Block,
        Wall,
        Null,
    }
}