﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Ning.Preferences;
using LitJson;
using UnityEngine;
using BlockBreak;
public class Global {

public static string filePath = Application.persistentDataPath + "/__data";
    public static readonly string version = "1.7.4";
    public const string dataFolderName = "Data/";
    public const string mapFileName = "map";
    public const string dataFileName = "content";


    public static DocHandler DocHandler = PreferencesFactory.DocHandler;
    public static PlayerPrefsHandler PlayerPrefsHandler = PreferencesFactory.PlayerPrefsHandler;

    public static UserGameData userGameData = new UserGameData();
    public static UserSetting setting = new UserSetting();
    public static BasicBallData basicBallData = new BasicBallData(DocHandler);
    public static SplashBallData splashBallData = new SplashBallData(DocHandler);
    public static SniperBallData sniperBallData = new SniperBallData();
    public static ScatterBallData scatterBallData = new ScatterBallData();
    public static CannonBallData cannonBallData = new CannonBallData();
    public static CancerBallData cancerBallData = new CancerBallData();
    

    //public static bool isPad = false;
    //public static bool isFullScene = false;

    //public static string SHA1(string content)
    //{
    //    try
    //    {
    //        SHA1 sha1 = new SHA1CryptoServiceProvider();
    //        byte[] bytes_in = Encoding.UTF8.GetBytes(content);
    //        byte[] bytes_out = sha1.ComputeHash(bytes_in);
    //        sha1.Clear();
    //        string result = BitConverter.ToString(bytes_out);
    //        result = result.Replace("-", "");
    //        return result.ToLower();
    //    }
    //    catch (Exception ex)
    //    {
    //        throw new Exception("SHA1加密出错：" + ex.Message);
    //    }
    //}
    

    public static void Save()
    {
        PreferencesFactory.Save();
    }

    public static int GetBallCount(BallType ballType)
    {
        var data = GetBallData(ballType);
        return data==null?0:data.Count;
    }

    public static int GetBallSpeedLevel(BallType ballType)
    {
        var data = GetBallData(ballType);
        return data==null?0:data.SpeedLevel;
    }

    public static int GetBallPowerLevel(BallType ballType)
    {
        var data = GetBallData(ballType);
        return data==null?0:data.PowerLevel;
    }

    public static IBallData GetBallData(BallType ballType)
    {
        switch (ballType)
        {
            case BallType.BasicBall:
                return basicBallData;
            case BallType.SplashBall:
                return splashBallData;
            // case BallType.SniperBall:
            //     return sniperBallData;
            // case BallType.ScatterBall:
            //     return basicBallData;
            // case BallType.CannonBall:
            //     return basicBallData;
            // case BallType.CancerBall:
            //     return basicBallData;
            default:
                return null;
        }
    }

#region UserGameData

    //主用户信息
    public class UserGameData
    {
        /// <summary>
        /// 起始的球的容量
        /// </summary>
        public int BaseBallCapacity = 50;

        /// <summary>
        /// 玩家分数
        /// </summary>
        public int Score
        {
            get
            {
                return DocHandler.GetInt("Score", 0);
            }
            set
            {
                DocHandler.SetInt("Score", value);
            }
        }

        /// <summary>
        /// 关卡等级
        /// </summary>
        public int Level
        {
            get
            {
                return DocHandler.GetInt("Level", 1);
            }
            set
            {
                DocHandler.SetInt("Level", value);
            }
        }

        /// <summary>
        /// 球总数量
        /// </summary>
        public int TotalBallCount
        {
            get
            {
                return  basicBallData.Count + 
                        splashBallData.Count + 
                        sniperBallData.Count + 
                        scatterBallData.Count + 
                        cannonBallData.Count + 
                        cancerBallData.Count ;
            }
        }

        /// <summary>
        /// 基础球数量
        /// </summary>
        public int BasicBallCount
        {
            get
            {
                return DocHandler.GetInt("BasicBallCount", 0);
            }
            set
            {
                DocHandler.SetInt("BasicBallCount", value);
            }
        }

        /// <summary>
        /// 溅射球数量
        /// </summary>
        public int SplashBallCount
        {
            get
            {
                return DocHandler.GetInt("SplashBallCount", 0);
            }
            set
            {
                DocHandler.SetInt("SplashBallCount", value);
            }
        }

        /// <summary>
        /// 狙击球数量
        /// </summary>
        public int SniperBallCount
        {
            get
            {
                return DocHandler.GetInt("SniperBallCount", 0);
            }
            set
            {
                DocHandler.SetInt("SniperBallCount", value);
            }
        }

        /// <summary>
        /// 分散球数量
        /// </summary>
        public int ScatterBallCount
        {
            get
            {
                return DocHandler.GetInt("ScatterBallCount", 0);
            }
            set
            {
                DocHandler.SetInt("ScatterBallCount", value);
            }
        }

        /// <summary>
        /// 大炮球数量
        /// </summary>
        public int CannonBallCount
        {
            get
            {
                return DocHandler.GetInt("CannonBallCount", 0);
            }
            set
            {
                DocHandler.SetInt("CannonBallCount", value);
            }
        }

        /// <summary>
        /// 癌球数量
        /// </summary>
        public int CancerBallCount
        {
            get
            {
                return DocHandler.GetInt("CancerBallCount", 0);
            }
            set
            {
                DocHandler.SetInt("CancerBallCount", value);
            }
        }
    } 
#endregion

#region UserSetting

    public class UserSetting
    {

        public Vector3? Text
        {
            get
            {
                return PlayerPrefsHandler.GetVector3("Text", Vector3.zero);
            }
            set
            {
                PlayerPrefsHandler.SetVector3("Text", value);
            }
        }
    }
#endregion

//TODO:目前只做两种球,剩下的有待完善
#region SniperBallData
    public class SniperBallData
    {
        public int Count
        {
            get
            {
                return DocHandler.GetInt("SniperBallCount", 0);
            }
            set
            {
                DocHandler.SetInt("SniperBallCount", value);
            }
        }
    }
#endregion

#region ScatterBallData
    public class ScatterBallData
    {
        public int Count
        {
            get
            {
                return DocHandler.GetInt("ScatterBallCount", 0);
            }
            set
            {
                DocHandler.SetInt("ScatterBallCount", value);
            }
        }
    }
#endregion

#region CannonBallData
    public class CannonBallData
    {
        public int Count
        {
            get
            {
                return DocHandler.GetInt("CannonBallCount", 0);
            }
            set
            {
                DocHandler.SetInt("CannonBallCount", value);
            }
        }
    }
#endregion

#region CancerBallData
    public class CancerBallData
    {
        public int Count
        {
            get
            {
                return DocHandler.GetInt("CancerBallCount", 0);
            }
            set
            {
                DocHandler.SetInt("CancerBallCount", value);
            }
        }
    }
#endregion

}


