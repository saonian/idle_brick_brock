using Ning.Preferences;

namespace BlockBreak
{
    public class BasicBallData : IBallData
    {
        private static DocHandler  DocHandler ;
        public BasicBallData( DocHandler _DocHandler)
        {
            DocHandler = _DocHandler;
        }
        public int Count
        {
            get
            {
                return DocHandler.GetInt("BasicBallCount", 0);
            }
            set
            {
                DocHandler.SetInt("BasicBallCount", value);
            }
        }

        public int SpeedLevel
        {
            get
            {
                return DocHandler.GetInt("BasicSpeedLevel", 0);
            }
            set
            {
                DocHandler.SetInt("BasicSpeedLevel", value);
            }
        }

        public int PowerLevel
        {
            get
            {
                return DocHandler.GetInt("BasicPowerLevel", 0);
            }
            set
            {
                DocHandler.SetInt("BasicPowerLevel", value);
            }
        }

        public void Reset() {
            SpeedLevel = 0;
            PowerLevel = 0;
            Count = 0;
        }
    }
}