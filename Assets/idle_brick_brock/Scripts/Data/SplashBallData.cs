using Ning.Preferences;

namespace BlockBreak
{
    public class SplashBallData : IBallData
    {
        private static DocHandler  DocHandler ;
        public SplashBallData( DocHandler _DocHandler)
        {
            DocHandler = _DocHandler;
        }
        public int Count
        {
            get
            {
                return DocHandler.GetInt("SplashBallCount", 0);
            }
            set
            {
                DocHandler.SetInt("SplashBallCount", value);
            }
        }
        /// <summary>
        /// 溅射球的速度等级影响的是溅射范围
        /// </summary>
        /// <value></value>
        public int SpeedLevel
        {
            get
            {
                return DocHandler.GetInt("SplashRangeLevel", 0);
            }
            set
            {
                DocHandler.SetInt("SplashRangeLevel", value);
            }
        }

        public int PowerLevel
        {
            get
            {
                return DocHandler.GetInt("SplashPowerLevel", 0);
            }
            set
            {
                DocHandler.SetInt("SplashPowerLevel", value);
            }
        }

        public void Reset() {
            SpeedLevel = 0;
            PowerLevel = 0;
            Count = 0;
        }
    }
}