namespace BlockBreak
{
    /// <summary>
    /// 球的数据接口
    /// </summary>
    public interface IBallData
    {
        int Count{get;set;}
        int SpeedLevel{get;set;}
        int PowerLevel{get;set;}

        void Reset();
    }
}