﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace BlockBreak
{
    /// <summary>
    /// 基础球的升级价格数据
    /// </summary>
    public class BallPriceData
    {
        
        /// <summary>
        /// 购买价格配置
        /// </summary>
        public Price buyPrice { get; set; }
        /// <summary>
        /// 速度升级价格配置
        /// </summary>
        public Price speed { get; set; }
        /// <summary>
        /// 力量升级价格配置
        /// </summary>
        public Price power { get; set; }
    }
}