﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using TMPro;
using Ning.Tools;
//using Text = TMPro.TextMeshProUGUI;

namespace BlockBreak
{
    public class UI_PanelSetting : Singleton<UI_PanelSetting>
    {
        
        private Button btn_close,btn_delete,btn_bg;
        private int delete_step = 0;
        protected override void Init() {
            btn_close = transform.Find("btn_close").GetComponent<Button>();
            btn_delete = transform.Find("btn_delete").GetComponent<Button>();
            btn_bg = transform.Find("bg").GetComponent<Button>();
            U.L(btn_close,btn_delete,btn_bg);
            btn_close.onClick.AddListener(Hide);
            btn_delete.onClick.AddListener(DeleteGameData);
            btn_bg.onClick.AddListener(ClickBG);
        }

        private void DeleteGameData()
        {
            if (delete_step == 0)
            {
                delete_step = 1;
            }
            else if (delete_step == 1)
            {
                delete_step = 0;
                //TODO:目前只做两种球,剩下的有待完善
                for (int i = 0; i < 2; i++)
                {
                    var ballData = Global.GetBallData((BallType)i);
                    Global.userGameData.Score = 0;
                    ballData.Reset();
                    StoreManager.Instance.Restart();
                    //TODO:重载地图
                    //TODO:方便开发,暂时不保存数据
                    RefreshEvent.Trigger();
                }

            }
            Refresh();
        }
        private void ClickBG()
        {
            if (delete_step == 1)
            {
                delete_step = 0;

            }
            Refresh();
        }

        public void Show()
        {
            Refresh();
            gameObject.SetActive(true);
        }

        public  void Hide()
        {
            gameObject.SetActive(false);
        }

        /// <summary>
        /// 刷新页面
        /// </summary>
        private void Refresh()
        {
            btn_delete.transform.Find("confirm").gameObject.SetActive(delete_step == 1);
        }
    }
}
