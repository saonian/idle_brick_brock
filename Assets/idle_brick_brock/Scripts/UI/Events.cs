﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using TMPro;
using Ning.Tools;
//using Text = TMPro.TextMeshProUGUI;

namespace BlockBreak
{
    /// <summary>
    /// 更新关卡等级
    /// </summary>
    public struct LevelEvent
    {
        public int Level;

        static LevelEvent e;
        public static void Trigger(int level)
        {
            e.Level = level;
            EventManager.TriggerEvent(e);
        }
    }


    /// <summary>
    /// 更新金币数
    /// </summary>
    public struct ScoreEvent
    {
        public int Score;

        static ScoreEvent e;
        public static void Trigger(int score)
        {
            e.Score = score;
            EventManager.TriggerEvent(e);
        }
    }

    /// <summary>
    /// 刷新UI
    /// </summary>
    public struct RefreshEvent
    {
        // public int Level;

        static RefreshEvent e;
        public static void Trigger()
        {
            // e.Level = level;
            EventManager.TriggerEvent(e);
        }
    }
}
