﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using TMPro;
using Ning.Tools;
//using Text = TMPro.TextMeshProUGUI;

namespace BlockBreak
{
    public class UI_TopBar : MonoBehaviour
        , EventListener<LevelEvent>
        , EventListener<ScoreEvent>
        , EventListener<RefreshEvent>
    {

        [Header("Text")]
        public Text txt_ballCount;
        public Text txt_level, txt_score;
        [Header("Button")]
        public Button btn_upgrades;
        public Button btn_setting;
        public Button[] btnBalls;

        private int[] ballsPrice = new int[]{0,0,0,0,0,0};
        void Start()
        {
            ScoreEvent.Trigger(0);
            for (int i = 0; i < btnBalls.Length; i++)
            {
                var type = (BallType)i;
                btnBalls[i].onClick.AddListener(() => {
                    BuyBall(type);
                });
            }
            btn_upgrades.onClick.AddListener(() => {
                UI_PanelUpgrades.Instance.Show();
            });
            btn_setting.onClick.AddListener(() => {
                UI_PanelSetting.Instance.Show();
            });

            Object Panel_setting = Resources.Load("Panel_setting", typeof(GameObject));
            GameObject Panel_settingObj = Instantiate(Panel_setting,transform.parent) as GameObject;
            Object Panel_upgrades = Resources.Load("Panel_upgrades", typeof(GameObject));
            GameObject Panel_upgradesObj = Instantiate(Panel_upgrades,transform.parent) as GameObject; 
            Panel_settingObj.SetActive(true);
            Panel_upgradesObj.SetActive(true);
            Panel_settingObj.SetActive(false);
            Panel_upgradesObj.SetActive(false);
            RefreshAll();
        }





        private void BuyBall(BallType type)
        {
            if (StoreManager.Instance.BuyBall(type))
            {
                RefreshAll();
            }
        }
        private void RefreshBallBtn()
        {
            for (int i = 0; i < btnBalls.Length; i++)
            {
                RefreshBallBtn(btnBalls[i], (BallType)i);
            }
        }



        //TODO:UI界面待优化,价格变化可采用进度条的方式展示,且可以作为画面质量设置的一样
        private void RefreshAll()
        {
            txt_score.text = Global.userGameData.Score.ToString();
            txt_ballCount.text = string.Format("{0} / {1}", Global.userGameData.TotalBallCount, Global.userGameData.BaseBallCapacity);
            for (int i = 0; i < btnBalls.Length; i++)
            {
                InitBtnBall(btnBalls[i], (BallType)i);
            }
        }

        private void InitBtnBall(Button btn, BallType type)
        {
            var txt_price = btn.transform.Find("Text").GetComponent<Text>();
            var price = StoreManager.Instance.GetBallPrice(type);
            txt_price.text = "$"+price.ToString();
            ballsPrice[(int)type] = price;
            RefreshBallBtn(btn,type);
        }

        private void RefreshBallBtn(Button btn, BallType type)
        {
            var hight = btn.transform.Find("hight").gameObject;
            hight.SetActive(ballsPrice[(int)type] < Global.userGameData.Score);
        }

        public void OnMMEvent(LevelEvent e)
        {
            U.L("Level : ",e.Level);
            txt_level.text = e.Level.ToString();
        }

        public void OnMMEvent(ScoreEvent e)
        {
            Global.userGameData.Score += e.Score;
            txt_score.text = Global.userGameData.Score.ToString();
            RefreshBallBtn();
        }
        public void OnMMEvent(RefreshEvent eventType)
        {
            RefreshAll();
        }
        private void OnEnable()
        {
            this.EventStartListening<LevelEvent>();
            this.EventStartListening<ScoreEvent>();
            this.EventStartListening<RefreshEvent>();

        }

        private void OnDisable()
        {
            this.EventStopListening<LevelEvent>();
            this.EventStopListening<ScoreEvent>();
            this.EventStopListening<RefreshEvent>();
        }
        
    }
}
