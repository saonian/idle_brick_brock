﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using TMPro;
using Ning.Tools;
//using Text = TMPro.TextMeshProUGUI;

namespace BlockBreak
{
    public class UI_PanelUpgrades : Singleton<UI_PanelUpgrades>
        , EventListener<ScoreEvent>

    {
        [Header("GameObject")]
        public GameObject basic_group;
        [Header("Button")]
        public Button btn_close;

        protected override void Init() {
            btn_close.onClick.AddListener(Hide);
            InitBasicGroup();
        }


        private Button btn_basic_speed,btn_basic_power,btn_basic_delete;
        private int basic_speed_price,basic_power_price;
        private void InitBasicGroup()
        {
            
            btn_basic_speed = basic_group.transform.Find("btn_speed").GetComponent<Button>();
            btn_basic_power = basic_group.transform.Find("btn_power").GetComponent<Button>();
            btn_basic_delete = basic_group.transform.Find("btn_delete").GetComponent<Button>();
            btn_basic_speed.onClick.AddListener(()=>{
                UpgraderSpeed(BallType.BasicBall,basic_speed_price,RefreshBasicGroup);
            });
            btn_basic_power.onClick.AddListener(()=>{
                UpgraderPower(BallType.BasicBall,basic_power_price,RefreshBasicGroup);
            });
            btn_basic_delete.onClick.AddListener(()=>{
                DeleteBall(BallType.BasicBall,RefreshBasicGroup);
            });
            RefreshBasicGroup();
        }
        private void RefreshBasicGroup()
        {
            //TODO: 可能需要做升级限制,到一定等级不能再升级了,暂时不需要
                U.L("RefreshBasicGroup------------");
            basic_speed_price = StoreManager.Instance.GetUpgraderSpeedPrice(BallType.BasicBall);
            basic_power_price = StoreManager.Instance.GetUpgraderPowerPrice(BallType.BasicBall);
            int speed_level = Global.GetBallSpeedLevel(BallType.BasicBall);
            int power_level = Global.GetBallPowerLevel(BallType.BasicBall);
            InitUpgraderBtn(btn_basic_speed,basic_speed_price,speed_level);
            InitUpgraderBtn(btn_basic_power,basic_power_price,power_level);

            var count = Global.GetBallCount(BallType.BasicBall);
            basic_group.transform.Find("txt_count").GetComponent<Text>().text = "x"+count;
            basic_group.SetActive(count != 0);
        }

        private void InitUpgraderBtn(Button btn,int price,int level)
        {
            var txt_score = btn.transform.Find("txt_score").GetComponent<Text>();
            var txt_level_current = btn.transform.Find("txt_level_current").GetComponent<Text>();
            var txt_level_next = btn.transform.Find("txt_level_next").GetComponent<Text>();
            txt_score.text = "$"+price;
            txt_level_current.text = level.ToString();
            txt_level_next.text = (level+1).ToString();
            InitUpgraderBtnHightState(btn, price);
        }
        private void InitUpgraderBtnHightState(Button btn,int price)
        {
            var hight = btn.transform.Find("hight").gameObject;
            hight.SetActive(price < Global.userGameData.Score);
        }

        private void UpgraderSpeed(BallType ballType,int currentPrice,Action endAction)
        {
            if (currentPrice <= Global.userGameData.Score)
            {
                if (StoreManager.Instance.UpgraderSpeed(ballType))
                {
                    endAction();
                }
            }
        }

        private void UpgraderPower(BallType ballType,int currentPrice,Action endAction)
        {
            if (currentPrice <= Global.userGameData.Score)
            {
                if (StoreManager.Instance.UpgraderPower(ballType))
                {
                    endAction();
                }
            }
        }

        private void DeleteBall(BallType ballType,Action endAction)
        {
                if (StoreManager.Instance.DeleteBall(ballType))
                {
                    endAction();
                }
        }

        public void Show()
        {
            Refresh();
            gameObject.SetActive(true);
        }

        public  void Hide()
        {
            gameObject.SetActive(false);
        }

        /// <summary>
        /// 刷新页面
        /// </summary>
        private void Refresh()
        {
            RefreshBasicGroup();
        }

        public void OnMMEvent(ScoreEvent eventType)
        {
            if (gameObject.activeSelf)
            {
                InitUpgraderBtnHightState(btn_basic_speed, basic_speed_price);
                InitUpgraderBtnHightState(btn_basic_power, basic_power_price);
            }
        }
        private void OnEnable()
        {
            this.EventStartListening<ScoreEvent>();
            U.L("BindEvent,ScoreEvent");
        }

        private void OnDisable()
        {
            this.EventStopListening<ScoreEvent>();
        }
    }
}
