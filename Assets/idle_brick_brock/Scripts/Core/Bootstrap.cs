﻿using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = Unity.Mathematics.Random;

namespace BlockBreak
{
    public sealed class Bootstrap
    {
        public static GameSettings Settings;

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
        public static void InitializeAfterSceneLoad()
        {
            var settingsGO = GameObject.Find("game");
            Settings = settingsGO?.GetComponent<GameSettings>();

            Scene scene = SceneManager.GetActiveScene();
            if (!Settings || scene.name != Settings.MainSceneName)
            {
                return;
            }
            Debug.Log("Game Start");
            if (!Settings)
            {
                Debug.LogError(" `GameSettings' could not be found");
                return;
            }

            MapManager.Instance.LoadMap();
            StoreManager.Instance.LoadBall();
        }
    }
}
