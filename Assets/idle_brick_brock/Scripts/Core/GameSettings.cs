﻿using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class GameSettings : MonoBehaviour {

    public string MainSceneName;
    public GameObject[] BallPrefabs;

    public GameObject BlockPrefab;

    //public float playerMoveSpeed = 15.0f;
    //public float playerFireCoolDown = 0.1f;
    //public float enemySpeed = 8.0f;
    //public float enemyShootRate = 1.0f;
    //public float playerCollisionRadius = 1.0f;
    //public float enemyCollisionRadius = 1.0f;
    public float2 playfield = new float2(8.2f, 3.6f);

    public int ballCount;
    public int brickCountX;                                     //The amount of bricks that will be spawned horizontally (Odd numbers are recommended)
    public int brickCountY;

    //public Shot PlayerShotPrefab;
    //public Shot EnemyShotPrefab;

    //public EnemySpawnSystemState EnemySpawnState;
    //public Faction EnemyFaction;


    public Color[] blockColors;

    public int ClickPower = 1;

  
}
