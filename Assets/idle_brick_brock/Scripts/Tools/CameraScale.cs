﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Playables;

public class CameraScale : MonoBehaviour
{
    void Awake()
    {
        var camera = GetComponent<Camera>();
        camera.backgroundColor = new Color(0, 0, 0);
        float orthographicSize = camera.orthographicSize;
        float aspectRatio = Screen.width * 1.0f / Screen.height;

        int ManualWidth = 1920;
        int ManualHeight = 1080;
        int manualHeight;
        if (System.Convert.ToSingle(Screen.height) / Screen.width > System.Convert.ToSingle(ManualHeight) / ManualWidth)
            manualHeight = Mathf.RoundToInt(System.Convert.ToSingle(ManualWidth) / Screen.width * Screen.height);
        else
            manualHeight = ManualHeight;
        //Camera camera = GetComponent<Camera>();
        float scale = System.Convert.ToSingle(manualHeight / 1080f);
        camera.orthographicSize*= scale;
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }
}