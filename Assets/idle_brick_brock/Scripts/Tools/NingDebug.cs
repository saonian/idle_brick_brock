﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public static class U
{
    public static void L(params object[] strs)
    {
        var str = System.DateTime.Now + " ==@@==> ";
        if (strs.Length != 0)
        {
            for (int i = 0; i < strs.Length; i++)
            {
                str += strs[i] + "      ";
            }
        }
        Debug.Log(str);
    }
}

public class MMTools_Ning : MonoBehaviour
{
    [MenuItem("My_Tools/NormalizeObj &Z")]
    public static void NormalizeObj()
    {
        GameObject[] selectObjs = Selection.gameObjects;
        int objCtn = selectObjs.Length;
        for (int i = 0; i < objCtn; i++)
        {
            selectObjs[i].transform.localScale = new Vector3(
                (float)System.Math.Round(selectObjs[i].transform.localScale.x, 2),
                (float)System.Math.Round(selectObjs[i].transform.localScale.y, 2),
                (float)System.Math.Round(selectObjs[i].transform.localScale.z, 2));
            print((float)System.Math.Floor(selectObjs[i].transform.localPosition.x));
            selectObjs[i].transform.localPosition = new Vector3(
                (float)System.Math.Floor(selectObjs[i].transform.localPosition.x),
                (float)System.Math.Round(selectObjs[i].transform.localPosition.y),
                (float)System.Math.Round(selectObjs[i].transform.localPosition.z));
            print(selectObjs[i].transform.localPosition.x);

        }
    }

}