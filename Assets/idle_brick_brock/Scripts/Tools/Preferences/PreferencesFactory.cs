﻿using UnityEngine;
using UnityEditor;
namespace Ning.Preferences
{
    public static class PreferencesFactory 
    {
        public static DocHandler DocHandler;
        public static PlayerPrefsHandler PlayerPrefsHandler;

        static PreferencesFactory()
        {
            DocHandler = new DocHandler();
            PlayerPrefsHandler = new PlayerPrefsHandler();
        }

        public static void Save()
        {
            DocHandler.Save();
            PlayerPrefsHandler.Save();
        }
    }
}