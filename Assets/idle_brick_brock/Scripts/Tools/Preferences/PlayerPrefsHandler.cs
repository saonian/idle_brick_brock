﻿using UnityEngine;

namespace Ning.Preferences
{
    /// <summary>
    /// Implementation of IPreferences interface for the standard Unity PlayerPrefs.
    ///Unity编辑器PlayerPrefs接口实现。 
    /// </summary>
    public  class PlayerPrefsHandler : IPreferences
    {

        /// <summary>
        /// Wrapper for the same method in PlayerPrefs.
        /// </summary>
        public override void DeleteAll()
        {
            PlayerPrefs.DeleteAll();
        }

        /// <summary>
        /// Wrapper for the same method in PlayerPrefs.
        /// </summary>
        public override void DeleteKey(string key)
        {
            PlayerPrefs.DeleteKey(key);
        }

        /// <summary>
        /// Wrapper for the same method in PlayerPrefs - note that useSecurePrefs is ignored.
        /// </summary>
        public override float GetFloat(string key, float defaultValue = 0.0f)
        {
            return PlayerPrefs.GetFloat(key, defaultValue);
        }

        /// <summary>
        /// Wrapper for the same method in PlayerPrefs - note that useSecurePrefs is ignored.
        /// </summary>
        public override int GetInt(string key, int defaultValue = 0)
        {
            return PlayerPrefs.GetInt(key, defaultValue);
        }

        /// <summary>
        /// Wrapper for the same method in PlayerPrefs - note that useSecurePrefs is ignored.
        /// </summary>
        public override string GetString(string key, string defaultValue = "")
        {
            return PlayerPrefs.GetString(key, defaultValue);
        }

        /// <summary>
        /// Get boolean preferences
        /// </summary>
        public override bool GetBool(string key, bool defaultValue = false)
        {
            var temp = PlayerPrefs.GetString(key, defaultValue.ToString().ToLower());
            return temp.ToLower() == true.ToString().ToLower();
        }

        /// <summary>
        /// Get Vector2 preferences
        /// </summary>
        public override Vector2? GetVector2(string key, Vector2? defaultValue = null)
        {
            var temp = PlayerPrefs.GetString(key, Vector2ToString(defaultValue));
            return StringToVector2(temp);
        }

        /// <summary>
        /// Get Vector3 preferences
        /// </summary>
        public override Vector3? GetVector3(string key, Vector3? defaultValue = null)
        {
            var temp = PlayerPrefs.GetString(key, Vector3ToString(defaultValue));
            return StringToVector3(temp);
        }

        /// <summary>
        /// Get Color preferences
        /// </summary>
        public override Color? GetColor(string key, Color? defaultValue = null)
        {
            var temp = PlayerPrefs.GetString(key, ColorToString(defaultValue));
            return StringToColor(temp);
        }

        /// <summary>
        /// Wrapper for the same method in PlayerPrefs.
        /// </summary>
        public override bool HasKey(string key)
        {
            return PlayerPrefs.HasKey(key);
        }

        /// <summary>
        /// Wrapper for the same method in PlayerPrefs.
        /// </summary>
        public override void Save()
        {
            PlayerPrefs.Save();
        }

        /// <summary>
        /// Wrapper for the same method in PlayerPrefs - note that useSecurePrefs is ignored.
        /// </summary>
        public override void SetFloat(string key, float value)
        {
            PlayerPrefs.SetFloat(key, value);
        }

        /// <summary>
        /// Wrapper for the same method in PlayerPrefs - note that useSecurePrefs is ignored.
        /// </summary>
        public override void SetInt(string key, int value)
        {
            PlayerPrefs.SetInt(key, value);
        }

        /// <summary>
        /// Wrapper for the same method in PlayerPrefs - note that useSecurePrefs is ignored.
        /// </summary>
        public override void SetString(string key, string value)
        {
            PlayerPrefs.SetString(key, value);
        }

        /// <summary>
        /// Set boolean preferences
        /// </summary>
        public override void SetBool(string key, bool value)
        {
            PlayerPrefs.SetString(key, value.ToString().ToLower());
        }

        /// <summary>
        /// Set Vector2 preferences
        /// </summary>
        public override void SetVector2(string key, Vector2? value)
        {
            PlayerPrefs.SetString(key, Vector2ToString(value));
        }

        /// <summary>
        /// Set Vector3 preferences
        /// </summary>
        public override void SetVector3(string key, Vector3? value)
        {
            PlayerPrefs.SetString(key, Vector3ToString(value));
        }

        /// <summary>
        /// Set Color preferences
        /// </summary>
        public override void SetColor(string key, Color? value)
        {
            PlayerPrefs.SetString(key, ColorToString(value));
        }
    }
}

