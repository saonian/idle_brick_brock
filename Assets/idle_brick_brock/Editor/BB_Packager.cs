﻿using UnityEditor;
using UnityEngine;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System;
using Ning.Tools;

namespace BlockBreak
{
    class BB_Packager
    {

        [MenuItem("Assets/加密文件", false, 81)]
        public static void Generate1BinaryFile()
        {
            UnityEngine.Object[] slecets = Selection.GetFiltered(typeof(UnityEngine.Object), SelectionMode.DeepAssets);
            foreach (UnityEngine.Object item in slecets)
            {
                string FilePath = AssetDatabase.GetAssetPath(item);
                var file = new FileStream(FilePath, FileMode.Open, FileAccess.Read);
                StreamReader sr = new StreamReader(file);
                var stringData = sr.ReadToEnd();
                var str = FilePath.Split('/');
                var fileName = str[str.Length - 1] + ".xx";
                var foldername = str[str.Length - 2];
                SaveLoadManager.Save(stringData, fileName, foldername);
            }
            AssetDatabase.Refresh();
        }

        [MenuItem("Assets/解密文件", false, 82)]
        public static void Generate1TxtFile()
        {
            UnityEngine.Object[] slecets = Selection.GetFiltered(typeof(UnityEngine.Object), SelectionMode.DeepAssets);
            foreach (UnityEngine.Object item in slecets)
            {
                string FilePath = AssetDatabase.GetAssetPath(item);
                var str = FilePath.Split('/');
                var foldername = str[str.Length - 2];
                string tmp = str[str.Length - 1];
                var fileName = tmp.Remove(tmp.LastIndexOf("."));
                var stringData = SaveLoadManager.Load(fileName, foldername) as string;
                string pathIni = FilePath + ".txt";
                //文件已经存在 删除文件
                if (File.Exists(pathIni))
                {
                    File.Delete(pathIni);
                }
                File.WriteAllText(pathIni, stringData);
            }
            AssetDatabase.Refresh();
        }
    }
}
