# 放置打砖块

## 项目介绍
本项目原型为flash游戏 Idle Breakout(放置打砖块),通过[unity DOTS技术](https://www.u3dc.com/archives/3509)实现一遍

[Idle Breakout网页版地址](http://www.crazygames.com/game/idle-breakout)



本项目主要是对unity DOTS的一次练手,目前为半成品,已实现基本核心功能,但是整个游戏流程还未跑通.

因为unity DOTS还是预览版,且API更新比较快,,所以**不会再在此版本继续开发**

后期会在Unity 2020 以后的版本再重新制作一遍

## 软件架构
使用unity新推出的DOTS技术,本项目采用的是混合模式(Hybrid ECS),就是ECS与现有的Component结合,这样开发起来比较快.如果对性能要求有提升可以考虑改为Pure ECS.

## 安装
unity版本 Unity 2018.2.3f1

*最好与此版本一致,不然一些库的引用会有报错*

## 使用说明


#### Idle Breakout的界面
![输入图片说明](https://images.gitee.com/uploads/images/2019/1212/173444_a7676dc7_490488.png "6778c41f760f46e89e38d650214d6ded.png")


#### 复刻的界面

![复刻的界面](https://images.gitee.com/uploads/images/2019/1212/173316_a9db9c91_490488.png "TIM截图20191212171550.png")



#### 方块摆放编辑器(EditMap场景)
![方块摆放编辑器](https://images.gitee.com/uploads/images/2019/1212/173328_b236f8a0_490488.png "TIM截图20191212171817.png")
 